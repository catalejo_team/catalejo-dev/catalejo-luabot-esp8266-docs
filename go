#!/bin/bash

PID_MKDOCS=
ANS="Options"

startMkdocs() {
  make serve-chuck-web &
  sleep 5
  PID_MKDOCS=$(ps -ef | grep "mkdocs" | grep -v "grep" | awk '{print $2}')
  echo "Mkdocs iniciado en el proceso: ${PID_MKDOCS}"
  make view &
}

killMkdocs() {
  PROCESS=$(kill -9 ${PID_MKDOCS})
  sleep 1
  zenity --info \
    --title="Ejecutando acción" \
    --text="Deteniendo procesos: ${PROCESS}"
}

menu() {
  ANS=$(
    zenity --info --title "Documentación del Kit" \
      --text "Servicio de documentación mkdocs" \
      --extra-button Iniciar \
      --extra-button Detener \
      --ok-label Salir
  )
  echo "$ANS"
}

option() {
  echo "$ANS"
  if [ "$ANS" = "Iniciar" ]; then
    startMkdocs
  elif [ "$ANS" = "Detener" ]; then
    killMkdocs
  else
    killMkdocs
    ANS="Salir"
  fi
}

while [ "$ANS" != "Salir" ]; do
  menu
  option
done
