# Mini-computadora NodeMCU

!!! Info "Escucha este audio para que aprendas sobre este importante componente del kit"
    
<audio controls style="width: 100%" >
  <source src="../../offline/audios/tarjeta-desarrollo.ogg" type="audio/ogg">
  Your browser does not support the audio element.
</audio>


## Configuración de los pines de la mini-computadora

![nodemcu-v3](../../img/esp8266/nodemcu-v3.png){ .img_med }

## Energizar tarjeta

### Energizar tarjeta con powerbank

![materiales](../../img/energize-card/powerbank.jpg){ .img_med }


<iframe width="560" height="315" src="https://www.youtube.com/embed/O9mnupLpM-0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Energizar tarjeta con baterías

![pilasAAA](../../img/energize-card/pilasAAA.jpg){ .img_med }

![Conexión correcta](../../img/energize-card/correcta-conexion.jpg){ .img_med }


<iframe width="560" height="315" src="https://www.youtube.com/embed/faTTyd7eBFI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
