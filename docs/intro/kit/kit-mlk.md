# Materiales del Kit
!!! info "Escucha el audio para explorar el Kit"

    <audio controls style="width: 100%">
      <source src="../../offline/audios/exploracion-del-kit.ogg" type="audio/ogg">
      Your browser does not support the audio element.
    </audio>

## Carta de Bienvenida a tu kit "Chicas STEAM"

Puedes obtener la cartade bienvenida a tu Kit escaneadno el **código QR** que encuentras en la parte de atrás de tu bitacora.

![bitacora qr](bitacora-qr.png)

Puedes también descargar la bitácora desde el siguiente enlace:

[:material-cursor-default-click:{.heartBig} Descarga la Carta de Bienvenida a tu Kit "Chicas STEAM" :material-cursor-default-click:{.heartBig}](https://drive.google.com/file/d/1g65k2MuvpkT5xKxjn6Pe9xLbX0WN6cQs/view){ .md-button .md-button--primary}

## Presentación Exploración del Kit

<div style="width: 100%;"><div style="position: relative; padding-bottom: 56.25%; padding-top: 0; height: 0;"><iframe frameborder="0" width="1300px" height="731.25px" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://view.genial.ly/60f10b91dd9a7a0d0488fbc9" type="text/html" allowscriptaccess="always" allowfullscreen="true" scrolling="yes" allownetworking="all"></iframe> </div> </div>




## Descripción de los componentes del Kit

Para adentrarse en el mundo de la electrónica de forma creativa incentivando la imaginación de los más pequeños, se recomienda el siguiente kit que permite mediante varias aplicaciones comprender el funcionamiento de cada uno de sus componentes. El kit contiene los siguiente componentes:

| Componente                                           | Descripción                                                  | Imagen                                 |
| ---------------------------------------------------- | ------------------------------------------------------------ | -------------------------------------- |
| **Transistor**                                | El transistor es un componente que nos permite aumentar la energía requerida para componentes más grandes como altavoces y motores. | ![](../../img/basic/2n2222.png)             |
| **Cable micro USB**                                  | El cable micro USB es necesario para transferir datos entre la PC o dar energía a la tarjeta de desarrollo. Se puede conectar en puertos USB disponibles en Cargadores, PowerBank y PC. | ![](../../img/accesories/cable_micro_usb.png)      |
| **Cable caimán caimán**                              | Este tipo de cable tiene en los extremos unas pinzas similares a la mandíbula de un caimán, permiten realizar puentes haciendo retención firme en sus conexiones. | ![](../../img/basic/cable_caiman.png)         |
| **Sensor de temperatura sumergible DS18B20**         | Este sensor permite tomar datos de temperatura hasta en sustancias líquidas, lo que permite determinar en que estado se puede encontrar dicha sustancia. | ![](../../img/sensores/DS18B20.png)              |
| **Sensor de humedad y temperatura DHT11**            | Este sensor captura datos de humedad y temperatura. Es decir se puede aplicar en actividades como invernaderos, agricultura, monitoreo de temperatura, etc. | ![](../../img/sensores/DHT11.png)                |
| **Cable jumper hembra-hembra**                       | Este tipo de conector macho-macho son de conexión rápida permiten conectar sensores o actuadores a la tarjeta de desarrollo. | ![](../../img/basic/cable_HH.png)             |
| **Cable jumper macho-hembra**                        | Este tipo de conector macho-hembra son de conexión rápida permiten conectar sensores o actuadores en la protoboard. | ![](../../img/basic/cable_MH.png)             |
| **Cable jumper macho-macho**                         | Este tipo de conector macho-macho son de conexión rápida permite realizar conexiones directamente en la protoboard entre diferentes componentes como actuadores, resistencias, etc. | ![](../../img/basic/cable_MM.png)             |
| **Sensor de ultrasonido HC-SR04**                    | El sensor de ultrasonido tiene por objetivo brindar información sobre la distancia a su alrededor, de esta manera los murciélagos se mantienen informados de los obstáculos en su entorno para así poder desplazarse de un lugar a otro. | ![](../../img/sensores/hc-sr04.png)              |
| **Fotoresistencia**                                  | Es una resistencia eléctrica la cual varía su valor en función de la cantidad de luz que incide sobre su superficie. | ![](../../img/sensores/fotoresistencia.png)      |
| **LED difuso**                                       | El LED es un componente que nos permite indicar por medio de la luz cambios de estado presentes en nuestros proyectos. Este tipo de led es difuso ya que evita que su luz se propage al exterior. | ![](../../img/actuadores/led-difuso.png)           |
| **Tarjeta NodeMCU**                                  | Es el cerebro de cada proyecto ya que permite procesar la información que recibe de cada sensor y controlar componentes como controladores, tiene disponible conexión WIFI para conexión con otros dispositivos como celulares, PC, etc. | ![](../../img/esp8266/nodemcu.png)              |
| **Potenciómetro**                                    | El potenciómetro es una resistencia variable por movimiento mecánico. Es decir, se puede variar su valor si aplicamos giros sobre él. | ![](../../img/sensores/potenciometro.png)        |
| **Protoboard**                                       | La protoboard es una placa con varios puntos de conexión que nos permite unir varios componentes en un solo lugar. | ![](../../img/basic/protoboard.png)           |
| **Resistencia**                                      | La resistencia es un elemento que se opone al paso de corriente, este tipo de resistencia ya tienen un valor establecido por defecto. | ![](../../img/basic/resistencia.png)          |
| **Sensor de humedad de suelo**                       | El sensor de humedad de suelo puede ser enterrado en el suelo y nos permite determinar que tan humedo se encuentra este. | ![](../../img/sensores/sensor_humedad_suelo.png) |
| **Servomotor SG90**                                  | El servomotor tiene la función de mover su mecanismo a un angulo determinado, su movimiento se asemeja al de un brazo humano. Con él se pueden mover cosas. | ![](../../img/actuadores/sg90.png)                 |
| **Módulo de detección de gotas de lluvia y humedad** | Este módulo permite indicar cuando en su superficie hay agua. Es decir, varia su resistencia en precensia de agua. | ![](../../img/sensores/detector_lluvia.png)      |
| **Parlante**                                         | El parlante es un componente que nos permite convertir la electricidad en sonido. | ![](../../img/actuadores//parlante.png)             |
| **Pulsador**                                         | El pulsador permite realizar un puente entre dos puntos de conexión mientras se mantenga pulsado. | ![](../../img/sensores/pulsador.png)             |
| **Caja de lápiz carboncillo** | Elemento ideal para plantear tu imaginación en papel mediante dibujos, pemitiendo plasmar las ideas sobre tu proyecto. | ![](../../img/material-creativo//carboncillos.png) |
| **Bloc de papel Origami** | El Origami permite estimular la imaginación mediante el plegado de papel sin necesidad de realizar cortes y pegados. | ![](../../img/material-creativo//block-origami.png) |
<!-- | **LED chorro**                                       | El LED es un componente que nos permite indicar por medio de la luz cambios de estado presentes en nuestros proyectos. Este tipo de led es de chorro ya que permite que su luz se propage al exterior. | ![](../../img/actuadores/led_chorro.png)           | -->
<!-- | **Temperas** | Las temperas tienen el objetivo de darle vida a tus ideas plasmadas en papel por medio de sus colores. Además permite personalizar la caja que complementa el kit. | ![](../../img/material-creativo//temperas.png) | -->
<!-- | **Bloc de notas** | Cada idea o proyecto en mente que se te ocurra, lo puedes anotar en este block de notas para que no se te olvide. Además, que será de gran utilidad para resolver algunos problemas. | ![](../../img/material-creativo//bloc-notas.png) | -->
<!-- | **Multimetro** | El multimetro es un instrumento de medición para magnitudes como corriente, voltaje y resistencia. Permite verificar el correcto funcionamiento de componentes electrónicos. | ![](../../img/basic/multimetro.png) | -->
<!-- | **Kit de herramientas** | En ocasiones es necesario utilizar herramientas para la construcción de nuestros proyectos. Por eso este kit es indispensable para dichas ocasiones. | ![](../../img/herramientas-varias/kit-herramientas.png) | -->
<!-- | **Caja** | Esta caja nos sirve para almacenar todos los componentes que contiene el kit y poderlos transportar a cualquier lugar. | ![](../../img/caja-stikers/caja-kit.png) | -->
<!-- | **Calcomania** | Las calcomanias le permiten personalizar la caja para que te identifique y te motive a seguir creando ideas. | ![](../../img/caja-stikers/calcomania.png) | -->

## Videos de Exploración del Kit

### Cables y protoboard

<iframe width="640" height="480" src="https://www.youtube.com/embed/x8VLAdykznw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Resistencias

<iframe width="640" height="480" src="https://www.youtube.com/embed/CuYk5MfqaB8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### LED

<iframe width="640" height="480" src="https://www.youtube.com/embed/IjmZW9ImkWk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Pulsadores e interruptores

<iframe width="640" height="480" src="https://www.youtube.com/embed/TO2YQIWtOeA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Transistores

<iframe width="640" height="480" src="https://www.youtube.com/embed/fOoNr6xAzEk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Potenciómetro

<iframe width="640" height="480" src="https://www.youtube.com/embed/IjmZW9ImkWk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>







