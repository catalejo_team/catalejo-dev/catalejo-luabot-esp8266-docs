# Materiales del Kit

<div style="width: 100%;"><div style="position: relative; padding-bottom: 56.25%; padding-top: 0; height: 0;"><iframe frameborder="0" width="1300px" height="731.25px" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://view.genial.ly/60f10b91dd9a7a0d0488fbc9" type="text/html" allowscriptaccess="always" allowfullscreen="true" scrolling="yes" allownetworking="all"></iframe> </div> </div>

## Descripción de los componentes del Kit

Para adentrarse en el mundo de la electrónica de forma creativa incentivando la imaginación de los más pequeños, se recomienda el siguiente kit que permite mediante varias aplicaciones comprender el funcionamiento de cada uno de sus componentes. El kit contiene los siguiente componentes:

<!-- ### Kit Básico -->

<!-- A continuación podrás conocer cada uno de los elementos de tu *kit básico*; como se llama y cuál es su función. -->

<!-- | Componente    | Imagen                                 | -->
<!-- | ---------------------------------------------------------------------------------------------------------------- | -------------------------------------- | -->
<!-- | **1 Tarjeta NodeMCU**: Es el cerebro de cada proyecto ya que permite procesar la información que recibe de cada sensor y controlar componentes como controladores, tiene disponible conexión WIFI para conexión con otros dispositivos como celulares, PC, etc. | ![](../../img/esp8266/nodemcu.png)              | -->
<!-- | **1 Cable micro USB**: El cable micro USB es necesario para transferir datos entre la PC o dar energía a la tarjeta de desarrollo. Se puede conectar en puertos USB disponibles en Cargadores, PowerBank y PC. | ![](../../img/accesories/cable_micro_usb.png)      | -->
<!-- | **4 Transistores 2n2222**: El transistor es un componente que nos permite aumentar la energía requerida para componentes más grandes como altavoces y motores. | ![](../../img/basic/2n2222.png)             | -->
<!-- | **2 Cable caimán caimán**: Este tipo de cable tiene en los extremos unas pinzas similares a la mandíbula de un caimán, permiten realizar puentes haciendo retención firme en sus conexiones. | ![](../../img/basic/cable_caiman.png)         | -->
<!-- | **10 Cables jumper hembra-hembra**: Este tipo de conector macho-macho son de conexión rápida permiten conectar sensores o actuadores a la tarjeta de desarrollo. | ![](../../img/basic/cable_HH.png)             | -->
<!-- | **10 Cables jumper macho-hembra**: Este tipo de conector macho-hembra son de conexión rápida permiten conectar sensores o actuadores en la protoboard. | ![](../../img/basic/cable_MH.png)             | -->
<!-- | **10 Cable jumper macho-macho**: Este tipo de conector macho-macho son de conexión rápida permite realizar conexiones directamente en la protoboard entre diferentes componentes como actuadores, resistencias, etc. | ![](../../img/basic/cable_MM.png)             | -->
<!-- | **1 Fotoresistencia**: Es una resistencia eléctrica la cual varía su valor en función de la cantidad de luz que incide sobre su superficie. | ![](../../img/sensores/fotoresistencia.png)      | -->
<!-- | **10 LEDs difuso**: El LED es un componente que nos permite indicar por medio de la luz cambios de estado presentes en nuestros proyectos. Este tipo de led es difuso ya que evita que su luz se propage al exterior. | ![](../../img/actuadores/led-difuso.png)           | -->
<!-- | **1 Potenciómetro**: El potenciómetro es una resistencia variable por movimiento mecánico. Es decir, se puede variar su valor si aplicamos giros sobre él. | ![](../../img/sensores/potenciometro.png)        | -->
<!-- | **1 Protoboard**: La protoboard es una placa con varios puntos de conexión que nos permite unir varios componentes en un solo lugar. | ![](../../img/basic/protoboard.png)           | -->
<!-- | **60 Resistencias**: La resistencia es un elemento que se opone al paso de corriente, este tipo de resistencia ya tienen un valor establecido por defecto. | ![](../../img/basic/resistencia.png)          | -->
<!-- | **2 Diodos**: El diodo es un componente electrónico que solo deja circular corriente en un único sentido. | ![](../../img/basic/diodo-1n4004.jpg)          | -->
<!-- | **4 Pulsadores**: El pulsador permite realizar un puente entre dos puntos de conexión mientras se mantenga pulsado. | ![](../../img/sensores/pulsador.png)             | -->
<!-- | **2 LEDs chorro**: El LED es un componente que nos permite indicar por medio de la luz cambios de estado presentes en nuestros proyectos. Este tipo de led es de chorro ya que permite que su luz se propage al exterior. | ![](../../img/actuadores/led_chorro.png)           | -->
<!-- | **1 Motor DC**: El motor nos permite convertir la energía eléctrica en movimiento. | ![](../../img/actuadores/motor-dc.jpeg)           | -->
<!-- | **1 Buzzer**: El buzzer es un componente que nos permite convertir la electricidad en sonido. | ![](../../img/actuadores/buzzer.jpg)             | -->
<!-- | **1 Caja**: Esta caja nos sirve para almacenar todos los componentes que contiene el kit y poderlos transportar a cualquier lugar. | ![](../../img/caja-stikers/caja-plastica.jpg) | -->
<!-- KIT COMPLETO -->
<!-- | **1 Lápiz ** | Elemento ideal para plantear tu imaginación en papel mediante dibujos, pemitiendo plasmar las ideas sobre tu proyecto. | ![](../../img/material-creativo/lapiz.jpg) | -->
<!-- | **1 Bloc de notas** | Cada idea o proyecto en mente que se te ocurra, lo puedes anotar en este block de notas para que no se te olvide. Además, que será de gran utilidad para resolver algunos problemas. | ![](../../img/material-creativo//bloc-notas.png) | -->

### Kit Completo

A continuación podrás conocer cada uno de los elementos de tu *kit completo*; como se llama y cuál es su función.

| Componente | Imagen                                 |
| ---------------------------------------------------------------------------------------------------------------- | -------------------------------------- |
| **Tarjeta NodeMCU**: Es el cerebro de cada proyecto ya que permite procesar la información que recibe de cada sensor y controlar componentes como controladores, tiene disponible conexión WIFI para conexión con otros dispositivos como celulares, PC, etc. | ![](../../img/esp8266/nodemcu.png)              |
| **Cable micro USB**: El cable micro USB es necesario para transferir datos entre la PC o dar energía a la tarjeta de desarrollo. Se puede conectar en puertos USB disponibles en Cargadores, PowerBank y PC. | ![](../../img/accesories/cable_micro_usb.png)      |
| **Protoboard**: La protoboard es una placa con varios puntos de conexión que nos permite unir varios componentes en un solo lugar. | ![](../../img/basic/protoboard.png)           |
| **Cable jumper hembra-hembra**: Este tipo de conector macho-macho son de conexión rápida permiten conectar sensores o actuadores a la tarjeta de desarrollo. | ![](../../img/basic/cable_HH.png)             |
| **Cable jumper macho-hembra**: Este tipo de conector macho-hembra son de conexión rápida permiten conectar sensores o actuadores en la protoboard. | ![](../../img/basic/cable_MH.png)             |
| **Cable jumper macho-macho**: Este tipo de conector macho-macho son de conexión rápida permite realizar conexiones directamente en la protoboard entre diferentes componentes como actuadores, resistencias, etc. | ![](../../img/basic/cable_MM.png)             |
| **LED difuso**: El LED es un componente que nos permite indicar por medio de la luz cambios de estado presentes en nuestros proyectos. Este tipo de led es difuso ya que evita que su luz se propage al exterior. | ![](../../img/actuadores/led-difuso.png)           |
| **LED chorro**: El LED es un componente que nos permite indicar por medio de la luz cambios de estado presentes en nuestros proyectos. Este tipo de led es de chorro ya que permite que su luz se propage al exterior. | ![](../../img/actuadores/led_chorro.png)           |
| **Resistencia**: La resistencia es un elemento que se opone al paso de corriente, este tipo de resistencia ya tienen un valor establecido por defecto. | ![](../../img/basic/resistencia.png)          |
| **Cable caimán caimán**: Este tipo de cable tiene en los extremos unas pinzas similares a la mandíbula de un caimán, permiten realizar puentes haciendo retención firme en sus conexiones. | ![](../../img/basic/cable_caiman.png)         |
| **Pulsador**: El pulsador permite realizar un puente entre dos puntos de conexión mientras se mantenga pulsado. | ![](../../img/sensores/pulsador.png)             |
| **Transistor**: El transistor es un componente que nos permite aumentar la energía requerida para componentes más grandes como altavoces y motores. | ![](../../img/basic/2n2222.png)             |
| **4 Diodos**: El diodo es un componente electrónico que solo deja circular corriente en un único sentido. | ![](../../img/basic/diodo-1n4004.jpg)          |
| **Fotoresistencia**: Es una resistencia eléctrica la cual varía su valor en función de la cantidad de luz que incide sobre su superficie. | ![](../../img/sensores/fotoresistencia.png)      |
| **Potenciómetro**: El potenciómetro es una resistencia variable por movimiento mecánico. Es decir, se puede variar su valor si aplicamos giros sobre él. | ![](../../img/sensores/potenciometro.png)        |
| **1 Motor DC**: El motor nos permite convertir la energía eléctrica en movimiento. | ![](../../img/actuadores/motor-dc.jpeg)           |
| **1 Buzzer**: El buzzer es un componente que nos permite convertir la electricidad en sonido. | ![](../../img/actuadores/buzzer.jpg)             |
| **1 Sensor de humedad y temperatura DHT11**: Este sensor captura datos de humedad y temperatura. Es decir se puede aplicar en actividades como invernaderos, agricultura, monitoreo de temperatura, etc. | ![](../../img/sensores/DHT11.png)                |
| **1 Sensor de temperatura sumergible DS18B20**: Este sensor permite tomar datos de temperatura hasta en sustancias líquidas, lo que permite determinar en que estado se puede encontrar dicha sustancia. | ![](../../img/sensores/DS18B20.png)              |
| **1 Sensor de ultrasonido HC-SR04**: El sensor de ultrasonido tiene por objetivo brindar información sobre la distancia a su alrededor, de esta manera los murciélagos se mantienen informados de los obstáculos en su entorno para así poder desplazarse de un lugar a otro. | ![](../../img/sensores/hc-sr04.png)              |
| **1 Sensor de humedad de suelo YL-100**: El sensor de humedad de suelo puede ser enterrado en el suelo y nos permite determinar que tan humedo se encuentra este. | ![](../../img/sensores/sensor_humedad_suelo.png) |
| **1 Módulo de detección de gotas de lluvia y humedad YL-83 **: Este módulo permite indicar cuando en su superficie hay agua. Es decir, varia su resistencia en precensia de agua. | ![](../../img/sensores/detector_lluvia.png)      |
| **1 Parlante bocina**: El parlante es un componente que nos permite convertir la electricidad en sonido. | ![](../../img/actuadores//parlante.png)             |
| **1 Servomotor SG90**: El servomotor tiene la función de mover su mecanismo a un angulo determinado, su movimiento se asemeja al de un brazo humano. Con él se pueden mover cosas. | ![](../../img/actuadores/sg90.png)                 |
| **1 Caja plástica**: Esta caja nos sirve para almacenar todos los componentes que contiene el kit y poderlos transportar a cualquier lugar. | ![](../../img/caja-stikers/caja-rimax-11.png) |

<!-- | **Kit de herramientas** | En ocasiones es necesario utilizar herramientas para la construcción de nuestros proyectos. Por eso este kit es indispensable para dichas ocasiones. | ![](../../img/herramientas-varias/kit-herramientas.png) | -->
<!-- | **Calcomania** | Las calcomanias le permiten personalizar la caja para que te identifique y te motive a seguir creando ideas. | ![](../../img/caja-stikers/calcomania.png) | -->
<!-- | **Multimetro** | El multimetro es un instrumento de medición para magnitudes como corriente, voltaje y resistencia. Permite verificar el correcto funcionamiento de componentes electrónicos. | ![](../../img/basic/multimetro.png) | -->
<!-- | **Bloc de notas** | Cada idea o proyecto en mente que se te ocurra, lo puedes anotar en este block de notas para que no se te olvide. Además, que será de gran utilidad para resolver algunos problemas. | ![](../../img/material-creativo//bloc-notas.png) | -->
<!-- | **Caja de lápiz carboncillo** | Elemento ideal para plantear tu imaginación en papel mediante dibujos, pemitiendo plasmar las ideas sobre tu proyecto. | ![](../../img/material-creativo//carboncillos.png) | -->
<!-- | **Bloc de papel Origami** | El Origami permite estimular la imaginación mediante el plegado de papel sin necesidad de realizar cortes y pegados. | ![](../../img/material-creativo//block-origami.png) | -->
<!-- | **Temperas** | Las temperas tienen el objetivo de darle vida a tus ideas plasmadas en papel por medio de sus colores. Además permite personalizar la caja que complementa el kit. | ![](../../img/material-creativo//temperas.png) | -->

## Exploración del Kit

### Vídeos para ayudar en la exploración del kit

[Cables y protoboard](https://www.youtube.com/watch?v=x8VLAdykznw)

<iframe width="640" height="480" src="https://www.youtube.com/embed/x8VLAdykznw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


[Resistencias](https://www.youtube.com/watch?v=CuYk5MfqaB8)

<iframe width="640" height="480" src="https://www.youtube.com/embed/CuYk5MfqaB8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[LED](https://www.youtube.com/watch?v=IjmZW9ImkWk)

<iframe width="640" height="480" src="https://www.youtube.com/embed/IjmZW9ImkWk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Pulsadores e interruptores](https://www.youtube.com/watch?v=TO2YQIWtOeA)

<iframe width="640" height="480" src="https://www.youtube.com/embed/TO2YQIWtOeA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Transistores](https://www.youtube.com/watch?v=fOoNr6xAzEk)

<iframe width="640" height="480" src="https://www.youtube.com/embed/fOoNr6xAzEk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Potenciómetro](https://www.youtube.com/watch?v=IjmZW9ImkWk)

<iframe width="640" height="480" src="https://www.youtube.com/embed/IjmZW9ImkWk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>





