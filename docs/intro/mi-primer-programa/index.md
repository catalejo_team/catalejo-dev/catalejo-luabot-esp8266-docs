# Mi primer programa con Catalejo Editor/Malokalabs

![Hola kit](Luabot.png){ width="100%" }

## Audio de apoyo sobre el reto

!!! Info "Escucha las recomenaciones de este reto en el siguiente audio"
    
    <audio controls style="width: 100%">
      <source src="../../offline/audios/mi-primer-led-programado.ogg" type="audio/ogg">
      Your browser does not support the audio element.
    </audio>

## Antes de realizar el reto:

!!! Info "Antes de realizar tu primer programa te invitamos a realizar la exploración de la aplicación MalokaLabs/Catalejo Editor."
 
    Es necesario que compredas:

    - [x] ¿Cuál es la aplicación MalokaLabs? (APK)
    - [x] ¿Cómo se crea y se editan proyectos en MalokaLabs?
    - [x] ¿Cómo conectarse a la mini-computadora nodeMCU?

    [:material-cursor-default-click: Explora MalokaLabs :material-cursor-default-click:](https://maloka.gitlab.io/malokalabsdocs/malokalabs-cards/posts/features-malokalabs-app/){ .md-button .md-button--primary .heart}

## Materiales

Para desarrollar éste ejemplo necesitarás:

|Cantidad|Nombre|Imagen|
|:-------------:|:-------------:|:-----:|
|1 | [Tarjeta NodeMCU](../../../nodemcu_v1/) con el [software instalado](../../../install/firmware/)| ![Imagen nodemcu v3](../../img/esp8266/nodemcu.png){: style="width:30%; background:white;"}|
|1 | Cable USB micro B| ![cable USB](../../img/accesories/usb-micro-b.jpg){: style="width:30%;"}|
|1 | Cargador de celular o [baterías](../../../intro/energize-card/) (PowerBank) | ![cargador de celular](../../img/accesories/charger-5v.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}|

## Construyamos nuestro circuito

![Montaje circuito](tarjeta-energizada-con-indicaciones-sobre-rst.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

## Construyamos nuestro programa

![mi primer programa](mi-primer-programa-catalejo-editor.png)

## Montaje programación y prueba

Sigue los pasos que encontrarás en el video para
que puedas hacer tu primer programa con la
micro-computadora nodemcu y Catalejo Editor/Malokalabs.

!!! Info "Recuerda que la contraseña de la red WiFi de Catalejo es 87654321"
    

<iframe width="560" height="315" src="https://www.youtube.com/embed/dFk3ZOOHFPA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

!!! Info "Cuando hayas enviado el programa deberás oprimir el botón **RST** y verificar el funcionamiento del LED."
