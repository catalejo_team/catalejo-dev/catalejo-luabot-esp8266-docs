# Aplicación AppInventor y Luabot

Se presentará a continuación una idea general para crear aplicaciones
*Android* capaces de darle ordenes a Luabot a través de WiFi. 

Para tal fin haga lo siguiente:


## Descargar el proyecto

Descargue el proyecto **myControl.aia** desde el siguiente link:

[Control Android para Luabot App-Inventor](./myControl.aia)

## Crear cuenta en appInventor

Abra una cuenta en **appInventor** el cual encontrará en el siguiente link:

[app inventor 2](http://ai2.appinventor.mit.edu/)

## Importar proyecto

Importe el proyecto **myControl.aia** desde su ordenador a appinventor:

![appInventorImport](../../img/appinventor/importar.png)

## Diseño y blóques (lógicas)

Al importar tendrá el resultado que verá a continuación. fíjese en los botones
que encontrará enmarcados en rojo. éstos botones **Diseño** y **Bloques** permitiran
pasar del diseño de la aplicación a la lógica de la aplicación, estudielos para
poder más adelante (después de hacer la prueba) las respectivas modificaciones.

![Aplicación importada](../../img/appinventor/aplicacion_design.png)

## Analizar la aplicación

Estudie el diseño y la lógica de la aplicación, más adelante le permitirá hacer
sus propias aplicaciones.

![Bloques de la aplicación](../../img/appinventor/app_inventor.png)

## Generar la apk e instalar en dispositivo Android

Tiene dos posibilidades para generar la apk, se muestra en la imagen una de ellas,
la intención es poder descargarla a su dispositivo *Android* para instalarla. Use 
aquella que se le facilite más.

![Generar APK](../../img/appinventor/generarApk.png)

## Crear e instalar algortimo en LuaBot

Haga el siguente algoritmo e instalelo en la tarjeta NodeMCU:

![Algoritmo Luabot](../../img/appinventor/nodemcu.jpg)

## Conectar APK

Al abrir la aplicación desde su dispositivo *Android* configure la **IP** y el **PORT** como
se ve en la imagen. Finalmente oprima las flechas y verifique el estado del LED azul de la tarjeta NodeMCU

![Conectar a LuaBot](../../img/appinventor/aplicacion.jpg)



