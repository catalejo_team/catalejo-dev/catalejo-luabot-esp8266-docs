# Reiniciar tarjeta NodeMCU a valores de fábrica

<iframe width="640" height="480" src="https://www.youtube.com/embed/_d-Xt5PduBo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Este tipo de reinicio dejará la tarjeta en las condiciones iniciales en las cuales recibiste tu tarjeta:

* Memoria de programas borrada.
* Se retomará el nombre de la red  WiFi como **catalejo-xxxx** (donde x representa 4 caracteres únicos de la tarjeta).
* La contraseña de la red WiFi seguirá siendo **87654321**.

## ¿Cuándo hacer uso de éste tipo de reinicio general?

* Cuando la tarjeta después de una programación no permite volver a conectarse a ella a través de WiFi.
* Cuando la tarjeta no es capaz de recibir un programa en su memoria a pesar de permitir la conexión por WiFi.
* Cuando la tarjeta después varios intentos no llega al 100 % del envío de programa (De manera inusual, teniendo como referencia que antes sí lo permitía)

## ¿Por qué puede suceder este tipo de problemas?

Las máquinas hacen o ejecutan las instrucciones que se le son entregadas en un guión. El sistema creado es lo suficientemente
capaz de entender un léxico y una sintaxis tan poderosa que nos permite programar sin necesidad de conocer el lenguaje de programación específico
permitiéndonos exclusivamente preocuparnos por el algoritmo (el paso a paso que queremos que se desarrolle). El problema se da porque
la programación además de lo anterior debe superar los problemas de semántica (significado) y de excepciones (como dividir un número entre 0)
y es responsabilidad del programador superar este dilema, es decir, esta tarjeta solo entiende de lenguajes formales y no naturales los
cuales se revisten de redundancia y ambigüedad. Por ello si se le pide a la máquina desarrollar alguna tarea que no termine en nada, el sistema entrará en pánico
y se reiniciará a través de un guardian que vigila la estabilidad de las tareas a ejecutar.

## ¿Cuál es el paso a paso para el reinicio general?

<!-- ![apng-reset-global](../../img/intro/video.apng){: style="width:100%; margin-left: auto; margin-right: auto; display: block"} -->

### Materiales

* Una resistencia de 330 Ohms, que en el código de colores sería NARANJA, NARANJA CAFÉ Y DORADO
* Una protoboard
* La tarjeta NodeMCU
* Energía a través del cables USB

### Pasos

1. Inserta en la protoboard la tarjeta NodeMCU.
2. Energiza la tarjeta NodeMCU.
3. Introduce la resistencia de 330 Ohms en la protoboard en la misma fila donde en la NodeMCU está etiquetado los pines de **RX** y **G**, guíate de la siguiente imagen:
![reset-global](../../img/intro/reset-global.jpeg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}
4. A continuación oprime el botón de reinicio **RST** y espera a que el LED azul de la tarjeta se vuelva a encender para finalmente apagarse.
5. Quita ahora la resistencia de la protoboard.
6. Vuelve a presionar el botón de reinicio **RST**, el LED azul se volverá a encender y esta vez se mantendrá así hasta la siguiente programación.
7. En éste punto ya has terminado, solo basta verificar que se esté generando la red **catalejo-xxxx** 

!!! Warning "¿No funcionó el reinicio general? Prueba lo siguiente:"
    Si no funciona el reinicio general de la tarjeta, en vez de usar la resistencia prueba con un cable macho-macho e introdúcelo en la protoboard, en el mismo lugar donde viene la resistencia (puedes retirar la resistencia).

![reinicio-cable-macho-macho](reiniciogeneral_bb.jpg)

!!! Info "¡TEN EN CUENTA!"
    Cómo fue mencionado al principio, los problemas presentados generalmente son debidos a la lógica que uso el programador para crear el programa. Acá te invito
    a que no envíes el programa que causó el problema ya que obtendrás el mismo resultado; estudía tu programa, revisa su lógica, asesórate y finalmente haz los
    cambios y vuelve a programar tu tarjeta.

Saludos,

Johnny
