# Ultrasonido HC-SR04

<iframe width="640" height="480" src="https://www.youtube.com/embed/NQPdouAhN1g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Ejemplos

### Conexiones

![conexiones](../img/sensores/ultrasonido/nodemcu_ultrasonido_bb.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

### Algoritmo

![algoritmo](../img/sensores/ultrasonido/ultrasonido.jpeg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

