# Instala Catalejo editor en Android

<img src="./img/interfaz.png" alt="./img/install/interfaz.png" style="width:100%;">

!!! Info "La aplicación funciona en Android 5.0 en adelante"

Descarga e instala la versión del catalejo editor para Android del siguiente link de sourceforge.

[Dale clic aquí para descargar catalejo-editor](https://sourceforge.net/projects/catalejo-editor-luabot/files/catalejo-luabot.apk)

O puedes leer el siguiente QR:

![QR para descargar Catalejo Editor para android](img/qr-catalejo-apk.jpg){ .img_small }

En el siguiente video tendrás un ejemplo de instalación.

<!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/PThchc-Jq9c" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

<iframe width="315" height="560" src="https://www.youtube.com/embed/V1Db4Yvl9ZQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

