# Instalación de firmware desde Windows

<iframe width="640" height="480" src="https://www.youtube.com/embed/QOX1wS1TPX4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Necesitaras lo siguiente:

|Cantidad|Nombre|Imagen|
|:-------------:|:-------------:|:-----:|
|1 |Cable USB micro B| ![cable USB](../img/accesories/usb-micro-b.jpg){: style="width:30%;"}|
|1 |Tarjeta nodemcu versión 3| ![Imagen nodemcu v3](../img/esp8266/nodemcu.png){: style="width:30%; background:white;"}|
|1 |Computador con Windows | ![windows 10](../img/otros/Windows-10-Cortana.jpg){: style="width:30%;"}


Para instalar el firmware en la tarjeta de desarrollo nodemcu versión 3 tendrás que realizar
los siguientes pasos:

1. **Instalar los driver** de la tarjeta para que el sistema operativo Windows la reconozca.
2. **Instalar** en tu sistema operativo un programa llamado **nodemcu-pyflasher**
el cual es capaz de subir el firmware a la tarjeta de desarrollo nodemcu.
3. **Subir el firmware** a la placa de desarrollo nodemcu.
4. **Probar el funcionamiento** del firmware cargado en la tarjeta de desarrollo nodemcu.

!!! Info "Si quieres instalar el firmware en muchas tarjetas, los pasos **1** y **2** solo se requieren hacer una **única** vez."
    
A continuación explicamos cada uno de los pasos a realizar:

## 1. Instalación de los drivers en Windows

Necesitaremos el driver de la familia CH34x; para obtenerlo deberás ir a esta página:

[página de descarga oficial](http://www.wch.cn/download/CH341SER_ZIP.html)

Allí dar clic en el botón de descarga como te mostramos en la siguiente imagen:

![descargar driver](../img/install/descargar-driver.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

Descargado el **paquete.zip** deberás descomprimirlo y ejecutar el archivo **SETUP.EXE** siguiendo los pasos:

![ejecutando el archivo setup.exe](../img/install/install-driver.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

A continuación conecta la tarjeta nodemcu al computador a través del cable USB y espera que sea reconocida
por el sistema operativo, podrás ver esta operación en la barra de aplicaciones a través de una notificación
que te indicará algo similar a lo que te compartimos en la siguiente imagen:

![Notificación de instalación del driver en windows](../img/install/instalar_CH340_windows_4.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

!!! Info "Guarda en tus notas el nombre del puerto por el cual se está comunicando, en la imagen anterior se trata del **COM14** para ti puede que sea otro número distinto al 14"
        

!!! Note "También puedes verificar el anterior proceso de instalación desde el administrador de dispositivos"
        

Si hemos llegado hasta aquí cumpliendo los anteriores pasos hemos instalado con éxito el driver, ya puedes ir al paso número 2.

!!! Warning "Si llegados a este punto no es reconocida la tajeta, intenta reiniciando el computador y reconectando de nuevo la tarjeta al puerto USB"
        

## 2. Instalación de nodemcu-pyflasher

**Nodemcu-pyflasher** es una aplicación que no requiere instalación, simplemente descargamos y ejecutamos.

Para descargar la aplicación para Windows deberás ir al siguiente link:

[NodeMCU-PyFlasher.exe](https://github.com/marcelstoer/nodemcu-pyflasher/releases/download/v5.0.0/NodeMCU-PyFlasher.exe)

Al ejecutar el **NodeMCU-PyFlasher.exe** nos cargará el programa de manera similar a la siguiente imagen:

![interfaz de pyflasher](../img/install/pyflasher-start-enh.jpg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

Llegados a este punto has terminado de instalar **NodeMCU-PyFlasher** correctamente, a demás, estos pasos
no requieren ser repetidos para programar muchas tarjetas NodeMCU, ya puedes pasar al paso 3.

!!! Warning "Si tienes problemas para instalar NodeMCU-PyFlasher considera lo siguiente:"
        Pueda que tu sistema operativo tenga problemas con la versión de la aplicación a
        intalar, ya sea por librerías o porque la arquitectura del computador es por ejemplo
        de 32 bits; en la siguiente página encontrarás varias versiones de la aplicación que
        podrás pobrar y quedarte con una que sí te funcione:
        [Pagina de otras versiones de NodeMCU-PyFlasher](https://github.com/marcelstoer/nodemcu-pyflasher/releases)

## 3. Subir el firmware a la tarjeta

Para obtener el firmware a instalar en la tarjeta nodemcu visita el siguiente link y dale clic en el botón de descarga

[Link de descarga del firmware catalejo-firmware.bin](https://sourceforge.net/projects/catalejo-editor-luabot/files/catalejo-firmware.bin/download)

!!! Info "Ten presente el lugar donde guardaste el archivo que acabas de descargar"
        
Después de descargar el firmware, conecta la tarjeta nodemcu con el cable USB al computador y abre la aplicación **NodeMCU-PyFlasher**.

A continuación deberás llenar los campos que muestra la interfaz de la aplicación NodeMCU-PyFlasher:

* **Serial Port**: COM (Con el que se reconozca la tarjeta a través del puerto serial)
* **Firmware**: el catalejo-firmware.bin en la dirección donde lo has guardado
* **Baud rate**: 230400 o 115200
*  **Erase flash**:  Yes

Te mostramos un ejemplo a seguir donde deberás poner el nombre del COM que tú anotaste anteriormente:

![configuración de interfaz](../img/install/configurar-interfaz-np.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

Finalmente deberás darle clic en el botón llamado **Flash NodeMCU** y esperar que termine el proceso, este proceso durará unas decenas de segundos:

![Flasheando nodemcu](../img/install/flasheando-nodemcu.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

!!! Info "Si el mensaje al final es Firmware successfully flashed significa que has instalado el firmware en la tarjeta nodemcu correctamente, ya puedes seguir el paso final"
        

!!! Warning "Si tienes problemas visita el siguiente foro y revisa las repuestas de los problemas y/o pon el tuyo, te ayudaremos en lo que nos sea posible"
        [Link del foro https://catalejo.flarum.cloud/t/subir-catalejo-firmware-bin](https://catalejo.flarum.cloud/t/subir-catalejo-firmware-bin)

## 4. Probar el funcionamiento

De la siguiente imagen identifica en la tarjeta el botón de reinicio **RST** y el **LED pin 4**:

![Tarjeta nodemcu identificando el boton de reset](../img/esp8266/nodemcu-v3.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

Teniendo la tarjeta nodemcu conectada al computador, deberás oprimir el botón de reinicio. A continuación verás como el LED empezará a destellar varias veces cada cuarto de segundo;
cuenta al menos 4 destellos antes de apagarse, si eso sucede, quiere decir que has subido el firmware correctamente.

Atentamente,

Equipo de desarrollo
