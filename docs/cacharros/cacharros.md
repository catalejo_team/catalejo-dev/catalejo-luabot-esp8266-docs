## Cacharros que inspiran

![cacharros que inspiran](Cacharros.png)

A continuación te compartimos información sobre algunos "cacharros que inspiran"; son artefactos que se pueden realizar con los elementos del kit. Te invitamos a realizarlos y a pensar en las modificaciones que tú les podrías realizar.

[:material-cursor-default-click:{.heartBig} Descargar :material-cursor-default-click:{.heartBig}](https://drive.google.com/drive/folders/1QX70SrebFo15EP4yfF9AAtTBUNsUNpdo?usp=sharing){ .md-button .md-button--primary} {==Carpeta de drive==}

<!-- ## Theremin -->

<!-- ![Theremin](theremin.png) -->

<!-- Crea el único instrumento musical que se “toca” sin ser tocado, hace uso de un sensor de ultrasonido que es capaz de detectar la distancia en la que se encuentran las manos respecto al instrumento. -->

<!-- [:material-cursor-default-click:{.heartBig} Theremin :material-cursor-default-click:{.heartBig}](https://cdn.discordapp.com/attachments/769219617197260851/785527074882322442/Theremin.pdf){ .md-button .md-button--primary} {==Tutorial de construcción del theremin==} -->

