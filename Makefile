.ONESHELL:

SHELL=/bin/bash

# Enviroment conda to activate
ENV=work
CONDA_ACTIVATE = source $$($$CONDA_EXE info --base)/etc/profile.d/conda.sh ; conda activate; conda activate $(ENV)

c?=mkdocs.ylm

help:
	@printf "########################\n"\
	"#        MKDOCS        #\n"\
	"########################\n\n"\
	"Éste directorio \"docs/\" es de documentación "\
	"el cual está escrito en markdown y puede ser compilado para html.\n\n"\
	"Instrucciones:\n"\
	"1°: Debe tener instalado mkdocs.\n"\
	"\tlink oficial: https://www.mkdocs.org/\n"\
	"\tlink github: https://github.com/mkdocs/mkdocs\n"\
	"\tEsto lo puede realizar con el siguiente comando:\n"\
	"\n"\
	"\t\t pip install -r requirements.txt\n"\
	"\n"\
	"Comandos:\n\n"\
	"\tmake config-catalejo-mkdocs\t => Configura la documentación para Catalejo Luna\n"\
	"\tmake config-maloka-mkdocs\t => Configura la documentación para MalokaLabs\n"\
	"\tmake serve-catalejo\t => Crea una servicio local con la documentación de Catalejo Luna\n"\
	"\tmake serve-mlk\t => Crea un servicio local con la documentación para MalokaLabs\n"\
	"\tmake serve:\t=> Inicia un servidor web dinámico para la previsualización\n"\
	"\tmake build:\t=> Construye HTML definitivo para empotrar en algún servidor web\n"\
	"\tmake view:\t=> Muestra una vista HTML previa en el navegador\n"\
	"\tmake pack\t=> Crear un docs.zip con la información de build\n"\
	"\tmake clear:\t=> Borra contenido en el directorio build/\n\n"\
	"Atentos saludos:\n"\
	"\tEquipo Catalejo+\n\n"\
	"\tExample:\n"\
	"\t\tmake build c=file.ylm\n"\
	"\t\tmake serve c=file.ylm\n"

b: build-catalejo-nwjs
s: serve-catalejo
p: pdf-view

rm_ln_of_index:
	rm -rf docs/index.md
	rm -rf docs/intro/kit/index.md

config-catalejo-mkdocs: rm_ln_of_index
	@echo "Creando index.md para documentación catalejo"
	ln -sr docs/index-catalejo.md docs/index.md
	ln -sr docs/intro/kit/kit-catalejo.md docs/intro/kit/index.md

config-maloka-mkdocs: rm_ln_of_index
	@echo "Creando index.md para documentación maloka"
	ln -sr docs/index-mlk.md docs/index.md
	ln -sr docs/intro/kit/kit-mlk.md docs/intro/kit/index.md

serve-:
	mkdocs serve -f $(c)

serve-mlk:
	mkdocs serve -f ./mkdocs-web-mlk.yml

create-yml-catalejo:
	cat ./cfg-yml/site-catalejo-web.yml ./cfg-yml/nav.yml ./cfg-yml/plugin.yml > mkdocs-web-catalejo.yml

create-yml-catalejo-nwjs:
	cat ./cfg-yml/site-catalejo-desktop.yml ./cfg-yml/nav.yml ./cfg-yml/plugin.yml > mkdocs-catalejo-nwjs.yml

serve-catalejo: create-yml-catalejo
	mkdocs serve -f ./mkdocs-web-catalejo.yml

serve-catalejo-nwjs: create-yml-catalejo-nwjs
	$(CONDA_ACTIVATE)
	mkdocs serve -f mkdocs-catalejo-nwjs.yml

build-catalejo: create-yml-catalejo
	$(CONDA_ACTIVATE)
	mkdocs build -c -f mkdocs-web-catalejo.yml

build-catalejo-nwjs: create-yml-catalejo-nwjs
	$(CONDA_ACTIVATE)
	mkdocs build -c -f mkdocs-catalejo-nwjs.yml

build-:
	mkdocs build -c -f $(c)

view:
	x-www-browser localhost:8000
	@printf "Sino se ha lanzado el navegador con el contenido "\
	"everifique que en otra terminal este corriendo el comando serve (make serve) "\
	"y luego refresque el navegador (F5).\n"

pdf-view:
	@evince ./build/pdf/document.pdf

pack:
	cd build && zip -r ../docs.zip *

clean:
	rm -rf build/*

.PHONY: build
