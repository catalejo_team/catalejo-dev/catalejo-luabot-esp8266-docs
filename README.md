# Documentación de luabot esp8266

Esta es la documentación de usuario para el proyecto catalejo-luna

## Deploy page

La página se despliega en el servidor de [gitlab.io](https://catalejo_team.gitlab.io/catalejo-dev/catalejo-luabot-esp8266-docs/)

Atentamente:

Catalejo+ Team

## mkdocs config files

### Configuración (Web, Apk, desktop)

Si se requiere que la documentación sea portada a un apk, escritorio o web deberá
aplicar alguno de estos comandos:

* mkdocs-app-android.yml -> Configuración para la apk de catalejo-editor-mobile
* mkdocs-web-catalejo.yml -> Configuración para servicio web de luabot
* mkdocs-web-mlk.yml -> Configuración web para malokalabs

Ejemplo:

* `make config-catalejo-mkdocs` => Configura la documentación para Catalejo Luna
* `make config-maloka-mkdocs` => Configura la documentación para MalokaLabs

### Prueba de servicio

Puede lanzar el servicio de prueba como sigue:

* `make serve-catalejo` => Crea una servicio local con la documentación de Catalejo Luna
* `make serve-mlk` => Crea un servicio local con la documentación para MalokaLabs

## Instalar herramientas

### Instalación de dependencias

```bash
sudo apt update
sudo apt install \
  chromium \
  zenity \
  -y
```

### Instalación de conda y variable de entorno work

Descarga de Miniconda e instalación:

```bash
cd && cd Descargas || cd Downloads
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh # Seguir las instrucciones y reiniciar la terminal
```
Variable de entorno work:

```bash
conda create --name work
conda activate work
conda install -c anaconda pip
```

Instalación de herramientas:

```bash
pip install -r requirements.txt
```

### A tener en cuenta mkdocs-with-pdf

Para renderizar con pdf hay que tener en cuenta lo siguiente:

* Error al renderizar con render_js, ver solución [acá](https://github.com/orzih/mkdocs-with-pdf/issues/79)
* Manejo de tamaño de imagenes en tablas, ver [acá](https://github.com/orzih/mkdocs-with-pdf/issues/79)

### Materials recursos

* [Hacer uso de emojis](https://squidfunk.github.io/mkdocs-material/reference/icons-emojis/?h=icon)

## Referencias

### Comandos útiles en CONDA

```bash
conda update -n base -c defaults conda # Actualizar conda
conda create --name <name_env>  # Crear un entorno
conda info --envs # listar los entornor en conda
conda activate name_env # activar un entorno
conda deactivate # desactivar un entorno en conda
conda remove --name ENV_NAME --all # remover un entorno
```
